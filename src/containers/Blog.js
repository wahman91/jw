import React, { Component } from "react";
import { withRouteData, Link } from "react-static";
//

class Test extends Component {
  render() {
    return (
      <div>
        <h1>It's blog time.</h1>
        <br />
        All Posts:
        <ul>
          {this.props.posts.map(post => (
            <li key={post.id}>
              <Link to={`/blog/post/${post.id}/`}>{post.title}</Link>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
export default withRouteData(Test);
