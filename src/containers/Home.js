import React, { Component } from "react";
import { withSiteData } from "react-static";
//
import logoImg from "../../assets/images/jw.jpg";
import sl1 from "../../assets/images/sl1.jpg";
import sl2 from "../../assets/images/sl2.jpg";
import vera2 from "../../assets/images/vera/2.jpg";
import vera3 from "../../assets/images/vera/3.jpg";
import vera4 from "../../assets/images/vera/4.jpg";
import vera6 from "../../assets/images/vera/6.jpg";
import vera7 from "../../assets/images/vera/7.jpg";
/** @jsx jsx */
import { jsx } from "@emotion/core";
import TrackVisibility from "react-on-screen";
import { middleShadow, container } from "../../constants";
class TextColorChange extends Component {
  render() {
    return (
      <div
        css={{
          display: "flex",
          justifyContent: "center",
          margin: "auto",
          flex: "1"
        }}
      >
        <h3
          css={{
            color: this.props.isVisible ? "pink" : "white",
            transition: "all 2s ease-out",
            fontSize: this.props.isVisible ? "50px" : "38px",
            marginTop: "0"
          }}
        >
          24/8 2018 - Vera kommer till världen!
        </h3>
        <h3
          css={{
            opacity: this.props.isVisible ? 1 : 0,
            transition: "all 3s ease-in",
            transitionDelay: "1s",
            fontSize: "50px",
            color: "red",
            marginTop: "0"
          }}
        >
          &nbsp;&lt;3
        </h3>
      </div>
    );
  }
}

class WahmanRoundedImage extends Component {
  render() {
    return (
      <div
        css={{
          margin: "1rem auto",
          background:
            "url('" + this.props.source + "') center center no-repeat",
          backgroundSize: "cover",
          height: "250px",
          width: "250px",
          borderRadius: "999px",
          border: "5px solid white"
        }}
      />
    );
  }
}

class WahmanSlider extends Component {
  render() {
    return (
      <img
        src={sl1}
        css={{
          transition: "all 1s ease-out",
          margin: "3rem 0",
          transform: this.props.isVisible ? "scale(1, 1)" : "scale(0.3, 0.3)",
          maxWidth: "80%",
          border: "5px solid white",
          borderRadius: "8px"
        }}
      />
    );
  }
}
class FadeInSkills extends Component {
  render() {
    return (
      <div
        css={{
          transition: "opacity " + this.props.fadeTime + "s ease-in-out",
          margin: "1rem",
          background: "white",
          color: "grey",
          opacity: this.props.isVisible ? 1 : 0,
          height: "240px",
          maxWidth: "80%",
          width: "180px",
          borderRadius: "5px",
          textAlign: "center",
          boxShadow: middleShadow,
          padding: "2rem"
        }}
      >
        <h3
          css={{
            color: "#23211f",
            fontSize: "24px"
          }}
        >
          {this.props.name ? this.props.name : "Loading"}
        </h3>
        <p>{this.props.desc ? this.props.desc : "Loading"}</p>
      </div>
    );
  }
}

class Home extends Component {
  render() {
    return (
      <div
        css={{
          textAlign: "center"
        }}
      >
        {/* Start */}
        <h1 style={{ textAlign: "center" }}>Joakim</h1>
        <img
          css={{
            borderRadius: "9999px",
            width: "300px",
            maxWidth: "80%"
          }}
          src={logoImg}
          alt=""
        />
        {/* Skills */}
        <div
          css={{
            marginTop: "3rem",
            marginBottom: "0",
            color: "white",
            width: "100%",
            padding: "3rem 0",
            background: "rgba(238, 243, 246, 1)",
            background:
              "-webkit-linear-gradient(top, rgba(238, 243, 246, 1) 0%, rgba(34,122,122,1) 100%)",
            background:
              "linear-gradient(to bottom, rgba(238, 243, 246, 1) 0%, rgba(34,122,122,1) 100%)"
          }}
        >
          <div
            css={{
              width: "80%",
              display: "flex",
              flexDirection: "row",
              flexWrap: "wrap",
              justifyContent: "center",
              maxWidth: container,
              margin: "auto"
            }}
          >
            <TrackVisibility partialVisibility>
              <FadeInSkills fadeTime={0.6} name={"CSS"} desc={"Tjenare tja"} />
            </TrackVisibility>
            <TrackVisibility partialVisibility>
              <FadeInSkills fadeTime={0.8} name={"CSS"} desc={"Tjenare tja"} />
            </TrackVisibility>
            <TrackVisibility partialVisibility>
              <FadeInSkills fadeTime={1} name={"CSS"} desc={"Tjenare tja"} />
            </TrackVisibility>
            <TrackVisibility partialVisibility>
              <FadeInSkills fadeTime={1.2} name={"CSS"} desc={"Tjenare tja"} />
            </TrackVisibility>
            <TrackVisibility partialVisibility>
              <FadeInSkills fadeTime={1.4} name={"CSS"} desc={"Tjenare tja"} />
            </TrackVisibility>
            <TrackVisibility partialVisibility>
              <FadeInSkills fadeTime={1.6} name={"CSS"} desc={"Tjenare tja"} />
            </TrackVisibility>
            <TrackVisibility partialVisibility>
              <FadeInSkills fadeTime={1.8} name={"CSS"} desc={"Tjenare tja"} />
            </TrackVisibility>
            <TrackVisibility partialVisibility>
              <FadeInSkills fadeTime={2} name={"CSS"} desc={"Tjenare tja"} />
            </TrackVisibility>
            <TrackVisibility partialVisibility>
              <FadeInSkills fadeTime={2.2} name={"CSS"} desc={"Tjenare tja"} />
            </TrackVisibility>
          </div>
        </div>

        {/* About me */}

        <div
          css={{
            marginTop: "0",
            background: "rgba(34,122,122,1)",
            background:
              "-webkit-linear-gradient(top, rgba(34,122,122,1) 0%, rgba(34,122,122,1) 100%)",
            background:
              "linear-gradient(to bottom, rgba(34,122,122,1) 0%, rgba(34,122,122,1) 100%)"
          }}
        >
          <h3
            css={{
              color: "white",
              fontSize: "38px",
              marginTop: "0"
            }}
          >
            8/6 2017 - Gifter mig med världens vackraste fru
          </h3>
          <TrackVisibility partialVisibility>
            <WahmanSlider />
          </TrackVisibility>
        </div>

        {/* Vera */}

        <div
          css={{
            marginTop: "0",
            paddingTop: "4rem",
            background: "rgba(34,122,122,1)",
            background:
              "-webkit-linear-gradient(top, rgba(34,122,122,1) 0%, rgba(34,122,122,1) 100%)",
            background:
              "linear-gradient(to bottom, rgba(34,122,122,1) 0%, rgba(34,122,122,1) 100%)"
          }}
        >
          <TrackVisibility partialVisibility>
            <TextColorChange />
            <div
              css={{
                margin: "auto",
                display: "flex",
                flexDirection: "row",
                flexWrap: "wrap-reverse",
                maxWidth: container
              }}
            >
              <WahmanRoundedImage source={vera2} />
              <WahmanRoundedImage source={vera3} />
              <WahmanRoundedImage source={vera4} />
              <WahmanRoundedImage source={vera6} />
              <WahmanRoundedImage source={vera7} />
            </div>
          </TrackVisibility>
        </div>
      </div>
    );
  }
}

export default Home;
